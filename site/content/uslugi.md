+++
date = "2017-01-02T11:59:47+01:00"
title = "Nie kreujemy produktów – rozwiązujemy problemy."
+++

<section
    class="container-fluid bgCover parallax height-50 vertical-center"
    style="background-color: #ccc; color: ; background-image: url(images/4961736018a798bff629b7e7071a22e5/problemy-1.jpg);">
    <div class="row">
      <div
          class=" col-md-8 col-md-offset-2"
          style="background-color: ; color: ; background-image: url();">
            <div class="row">
                <div
                    class="content  font-44 weight-300"
                    style="background-color: ; color: #fff; background-image: url();">
                    <h2><p>Nie kreujemy produktów</p><p>– rozwiązujemy problemy.</p></h2>
                </div>
            </div>
      </div>
    </div>
</section>
<section
    class="webrain moreColumns container-fluid container height-70 pdb-5 pdt-5 vertical-top"
    style="background-color: ; color: ; background-image: url();">
    <div class="row">

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-star-o iconMargin-10 iconPadding-14 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Zespół
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Nasz zespół architektów aplikacji, projektantów i programistów, kierowany przez managerów projektów jest gotowy, aby odpowiednio opisać i uporządkować procesy, i zrealizować je w bezpiecznych i wygodnych aplikacjach internetowych i mobilnych.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-heart iconMargin-10 iconPadding-14 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Technologie
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Obserwujemy trendy, zbieramy wspólne doświadczenie, uczymy się od innych – nasze aplikacje wykorzystują najnowsze rozwiązania programistyczne, wspierane i rozwijane przez społeczności developerskie na całym świecie.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-user iconMargin-10 iconPadding-14 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Usługi
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Oferujemy usługę kompleksową: od projektu aplikacji, programowanie i testowanie, po migrację danych, wdrożenie i uruchomienie produkcyjne, oraz wsparcie techniczne i utrzymanie aplikacji podczas jej życia.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-user iconMargin-10 iconPadding-14 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Usługi
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Oferujemy usługę kompleksową: od projektu aplikacji, programowanie i testowanie, po migrację danych, wdrożenie i uruchomienie produkcyjne, oraz wsparcie techniczne i utrzymanie aplikacji podczas jej życia.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-user iconMargin-10 iconPadding-14 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Usługi
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Oferujemy usługę kompleksową: od projektu aplikacji, programowanie i testowanie, po migrację danych, wdrożenie i uruchomienie produkcyjne, oraz wsparcie techniczne i utrzymanie aplikacji podczas jej życia.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-user iconMargin-10 iconPadding-14 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Usługi
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Oferujemy usługę kompleksową: od projektu aplikacji, programowanie i testowanie, po migrację danych, wdrożenie i uruchomienie produkcyjne, oraz wsparcie techniczne i utrzymanie aplikacji podczas jej życia.
                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain container-fluid moreColumns container height-10 pdb-5 pdt-5 vertical-center"
    style="background-color: #eee; color: ; background-image: url();">
    <div class="row">

            <div
                class="bgCover col-md-14 col-sm-12 col-xs-12 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-32 weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Jak wygląda proces realizacji?
                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain moreColumns container-fluid container timeline height-80 pdb-5 pdl-5 pdt-5 vertical-top"
    style="background-color: #eee; color: ; background-image: url();">
    <div class="row">

            <div
                class="bgCover col-md-14 col-sm-12 col-xs-12 pdb-5 pdl-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Przeniesienie na docelowe środowisko
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Nasz zespół architektów aplikacji, projektantów i programistów, kierowany przez managerów projektów jest gotowy, aby odpowiednio opisać i uporządkować procesy, i zrealizować je w bezpiecznych i wygodnych aplikacjach internetowych i mobilnych.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-14 col-sm-12 col-xs-12 pdb-5 pdl-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Przeniesienie na docelowe środowisko
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Nasz zespół architektów aplikacji, projektantów i programistów, kierowany przez managerów projektów jest gotowy, aby odpowiednio opisać i uporządkować procesy, i zrealizować je w bezpiecznych i wygodnych aplikacjach internetowych i mobilnych.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-14 col-sm-12 col-xs-12 pdb-5 pdl-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Przeniesienie na docelowe środowisko
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Nasz zespół architektów aplikacji, projektantów i programistów, kierowany przez managerów projektów jest gotowy, aby odpowiednio opisać i uporządkować procesy, i zrealizować je w bezpiecznych i wygodnych aplikacjach internetowych i mobilnych.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-14 col-sm-12 col-xs-12 pdb-5 pdl-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Przeniesienie na docelowe środowisko
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Nasz zespół architektów aplikacji, projektantów i programistów, kierowany przez managerów projektów jest gotowy, aby odpowiednio opisać i uporządkować procesy, i zrealizować je w bezpiecznych i wygodnych aplikacjach internetowych i mobilnych.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-14 col-sm-12 col-xs-12 pdb-5 pdl-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Przeniesienie na docelowe środowisko
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Nasz zespół architektów aplikacji, projektantów i programistów, kierowany przez managerów projektów jest gotowy, aby odpowiednio opisać i uporządkować procesy, i zrealizować je w bezpiecznych i wygodnych aplikacjach internetowych i mobilnych.
                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain container-fluid bgCover height-40 vertical-center"
    style="background-color: #ccc; color: ; background-image: url(images/4961736018a798bff629b7e7071a22e5/projekty-new.jpg);">
    <div class="row">

            <div
                class=" col-md-8 col-md-offset-2 text-left"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-44 weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <h2>O nas</h2>

                    </div>

                </div>
            </div>

    </div>
</section>
