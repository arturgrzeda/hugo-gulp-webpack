+++
date = "2017-01-02T11:59:47+01:00"
title = "Nie kreujemy produktów – rozwiązujemy problemy."
+++

<section
    class="webrain container-fluid bgCover parallax height-50 pdt-5 vertical-center"
    style="background-color: #ccc; color: ; background-image: url(images/6e86d35dea440d4aa821fa4be994b837/projekty-new-1.jpg);">
    <div class="row">

            <div
                class=" col-md-8 col-md-offset-2"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-44 weight-300"
                        style="background-color: ; color: #fff; background-image: url();">
                        <h2><p>Zrealizowane projekty</p><p>- case studies</p></h2>

                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain container-fluid"
    style="background-color: ; color: ; background-image: url();">
    <div class="row">

            <div
                class="parallax bgCover col-md-5 col-md-offset-1 col-sm-12 col-xs-12 height-50 mb-5 mt-5 pdl-5 pdr-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-30 weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <h3>Union Investment</h3>

                    </div>

                    <div
                        class="content  font-20 weight-300"
                        style="background-color: ; color: ; background-image: url();">
                        <i></i>System obiegu elektronicznego dokumentów
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Dzięki zastosowaniu naszej aplikacji intranetowej do obiegu dokumentów i informacji, oraz rozszerzeniu jej funkcjonalności o integrację z planem kont programu księgowego udało nam się zoptymalizować i udrożnić pracę ponad 100 pracowników firmy.
                    </div>

                </div>
            </div>

            <div
                class="bgCover bg-center col-md-5 col-sm-12 col-xs-12 height-50 mb-5 mt-5 vertical-center"
                style="background-color: ; color: ; background-image: url(images/6e86d35dea440d4aa821fa4be994b837/union-in-7.jpg);">
                <div class="row">

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        <br>
                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain container-fluid"
    style="background-color: #232323; color: ; background-image: url();">
    <div class="row">

            <div
                class="bgCover bg-center col-md-5 col-md-offset-1 col-sm-12 col-xs-12 height-50 mb-5 mt-5 vertical-center"
                style="background-color: ; color: ; background-image: url(images/6e86d35dea440d4aa821fa4be994b837/itask-2.jpg);">
                <div class="row">

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        <br>
                    </div>

                </div>
            </div>

            <div
                class="parallax bgCover col-md-5 col-sm-12 col-xs-12 height-50 mb-5 mt-5 pdl-5 pdr-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-30 weight-300"
                        style="background-color: ; color: #fff; background-image: url();">
                        <h3>iTask</h3>

                    </div>

                    <div
                        class="content  font-20 weight-300"
                        style="background-color: ; color: #999; background-image: url();">
                        <i></i>System zarządzania projektami
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: #999; background-image: url();">
                        Stworzyliśmy autorskie rozwiązanie oparte na metodologii SCRUM, które pozwala na planowanie i zarządzanie dużą liczbą projektów o różnym poziomie skomplikowania.
                    </div>

                </div>
            </div>

    </div>
</section>
