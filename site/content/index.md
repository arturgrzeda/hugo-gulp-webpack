+++
date = "2016-12-31T17:31:10+01:00"
title = "index"
+++

<section
    class="container-fluid bg-center height-100 repeat-no-repeat vertical-center"
    style="background-color: ; color: ; background-image: url();">
    <div class="row">

            <div
                class=" col-md-8 col-md-offset-2"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-40 text-center weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <h2>Tworzymy i utrzymujemy <b>aplikacje internetowe</b> oraz <b>mobilne</b>, wspierające funkcje i procesy biznesowe.</h2>

                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class=" masonry moreColumns height-20 masonryCol-3 vertical-center"
    style="background-color: ; color: ; background-image: url();">
    <div class="row">

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-80 vertical-center"
                style="background-color: ; color: ; background-image: url(images/d1546d731a9f30cc80127d57142a482b/union-in-11901x901.jpg);">
                <div class="row">

                    <div
                        class="content  font-16 weight-500"
                        style="background-color: ; color: ; background-image: url();">
                        <h3><br></h3>

                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-40 vertical-center"
                style="background-color: ; color: ; background-image: url(images/d1546d731a9f30cc80127d57142a482b/itask-2420x420.jpg);">
                <div class="row">

                    <div
                        class="content  font-16 weight-500"
                        style="background-color: ; color: ; background-image: url();">
                        <h3><br></h3>

                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-40 vertical-center"
                style="background-color: ; color: ; background-image: url(images/d1546d731a9f30cc80127d57142a482b/gettable-10420x420.jpg);">
                <div class="row">

                    <div
                        class="content  font-16 weight-500"
                        style="background-color: ; color: ; background-image: url();">
                        <h3><br></h3>

                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-40 vertical-center"
                style="background-color: ; color: ; background-image: url(images/d1546d731a9f30cc80127d57142a482b/votee-3420x420.jpg);">
                <div class="row">

                    <div
                        class="content  font-16 weight-500"
                        style="background-color: ; color: ; background-image: url();">
                        <h3><br></h3>

                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-40 vertical-center"
                style="background-color: ; color: ; background-image: url(images/d1546d731a9f30cc80127d57142a482b/3-dodaj-projekt420x420.png);">
                <div class="row">

                    <div
                        class="content  font-16 weight-500"
                        style="background-color: ; color: ; background-image: url();">
                        <h3><br></h3>

                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class=" container-fluid height-50 vertical-center"
    style="background-color: #1E2229; color: ; background-image: url();">
    <div class="row">

            <div
                class=" col-md-8 col-md-offset-2"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-40 text-center weight-100"
                        style="background-color: ; color: #fff; background-image: url();">
                        <h2>Tworzymy i utrzymujemy <b>aplikacje internetowe</b> oraz <b>mobilne</b>, wspierające funkcje i procesy biznesowe.</h2>

                    </div>

                </div>
            </div>

    </div>
</section>
