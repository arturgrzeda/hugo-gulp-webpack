+++
date = "2017-01-02T11:59:47+01:00"
title = "Nie kreujemy produktów – rozwiązujemy problemy."
+++

<section
    class="webrain container-fluid bgCover parallax height-50 pdt-5 vertical-center"
    style="background-color: #ccc; color: ; background-image: url(images/58ab536750bf7b61a2448ba0ec4898d6/o-nas-new5-1.jpg);">
    <div class="row">

            <div
                class=" col-md-8 col-md-offset-2"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-44 weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <h2>O nas</h2>

                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain container-fluid"
    style="background-color: ; color: ; background-image: url();">
    <div class="row">

            <div
                class="parallax bgCover col-md-4 col-md-offset-2 col-sm-12 col-xs-12 height-50 pdr-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-30 weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <h3>Kim jesteśmy</h3>

                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Firma Vitamin powstała w 2005 roku w odpowiedzi na rosnące zapotrzebowanie na usługi tworzenia stron i prostych aplikacji internetowych. Realizacja usług outsourcingu IT w firmach księgowych i HR, I zapotrzebowanie na innowacyjne I skuteczne rozwiązania wymiany i przetwarzania informacji stały się dodatkową motywacją, aby nie poprzestać na statycznych stronach www, a rozszerzyć je lub w całości wykorzystać tę technologię jako doskonały sposób wymiany informacji.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-6 col-sm-12 col-xs-12 height-50 vertical-center"
                style="background-color: ; color: ; background-image: url(images/58ab536750bf7b61a2448ba0ec4898d6/kim.jpg);">
                <div class="row">

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        <br>
                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain container-fluid"
    style="background-color: ; color: ; background-image: url();">
    <div class="row">

            <div
                class="bgCover col-md-6 col-sm-12 col-xs-12 height-50 vertical-center"
                style="background-color: ; color: ; background-image: url(images/58ab536750bf7b61a2448ba0ec4898d6/biznes-2.jpg);">
                <div class="row">

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        <br>
                    </div>

                </div>
            </div>

            <div
                class="parallax bgCover col-md-4 col-sm-12 col-xs-12 height-50 pdl-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-30 weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <h3>Jak możemy wesprzeć Twój biznes?</h3>

                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Brak spójnej bazy klientów, problemy z akceptacją dokumentów, chęć rozszerzenia sprzedaży produktu w Internecie czy problemy komunikacyjne i nieoptymalna obsługa istniejących usług – tego typu sytuacje zdarzają się bądź są codziennością w większości firm. Odpowiednie ich rozwiązanie w znaczący sposób wpływa na poziom kontroli i komfort pracy, czego efektem jest rozwój firmy i zwiększenie dochodów.
                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain masonry moreColumns container container-fluid height-70 masonryCol-3 pdb-5 pdt-5 vertical-top"
    style="background-color: #eee; color: ; background-image: url();">
    <div class="row">

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-60 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-30 weight-200"
                        style="background-color: ; color: #000; background-image: url();">
                        <h3>Więcej o nas<br></h3>

                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-star-o iconMargin-14 iconPadding-12 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Zespół
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Nasz zespół architektów aplikacji, projektantów i programistów, kierowany przez managerów projektów jest gotowy, aby odpowiednio opisać i uporządkować procesy, i zrealizować je w bezpiecznych i wygodnych aplikacjach internetowych i mobilnych.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-heart iconMargin-14 iconPadding-12 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Technologie
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Obserwujemy trendy, zbieramy wspólne doświadczenie, uczymy się od innych – nasze aplikacje wykorzystują najnowsze rozwiązania programistyczne, wspierane i rozwijane przez społeczności developerskie na całym świecie.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-4 col-sm-12 col-xs-12 height-30 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-16 icon-user iconMargin-14 iconPadding-12 iconSize-45 weight-400"
                        style="background-color: ; color: #000; background-image: url();">
                        <i></i>Usługi
                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Oferujemy usługę kompleksową: od projektu aplikacji, programowanie i testowanie, po migrację danych, wdrożenie i uruchomienie produkcyjne, oraz wsparcie techniczne i utrzymanie aplikacji podczas jej życia.
                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain container-fluid"
    style="background-color: ; color: ; background-image: url();">
    <div class="row">

            <div
                class="parallax bgCover col-md-4 col-md-offset-2 col-sm-12 col-xs-12 height-50 pdl-5 pdr-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-30 weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <h3><p>Akademia Vitamin</p></h3>

                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Kluczową kwestią w naszej firmie jest rozwój naszego zespołu – pracownicy poszczególnych specjalności spotykają się co najmniej raz w tygodniu, aby dyskutować o trendach i nowych rozwiązaniach, ale również o problemach, o których wiedzą, bądź które sami napotykają. Dzięki stałemu rozwojowi nie mamy problemów ze zdobywaniem i tworzeniem nowoczesnych projektów. A co równie istotne, dzięki niej do naszego zespołu chcą dołączać bardzo dobrzy specjaliści, dzięki którym jesteśmy coraz lepsi.
                    </div>

                </div>
            </div>

            <div
                class="bgCover col-md-6 col-sm-12 col-xs-12 height-50 vertical-center"
                style="background-color: ; color: ; background-image: url(images/58ab536750bf7b61a2448ba0ec4898d6/akademia.jpg);">
                <div class="row">

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        <br>
                    </div>

                </div>
            </div>

    </div>
</section>

<section
    class="webrain container-fluid"
    style="background-color: ; color: ; background-image: url();">
    <div class="row">

            <div
                class="bgCover col-md-6 col-sm-12 col-xs-12 height-50 vertical-center"
                style="background-color: ; color: ; background-image: url(images/58ab536750bf7b61a2448ba0ec4898d6/dolacz.jpg);">
                <div class="row">

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        <br>
                    </div>

                </div>
            </div>

            <div
                class="parallax bgCover col-md-4 col-sm-12 col-xs-12 height-50 pdl-5 pdr-5 vertical-center"
                style="background-color: ; color: ; background-image: url();">
                <div class="row">

                    <div
                        class="content  font-30 weight-300"
                        style="background-color: ; color: #000; background-image: url();">
                        <h3>Dołącz do nas</h3>

                    </div>

                    <div
                        class="content "
                        style="background-color: ; color: ; background-image: url();">
                        Najważniejszym zasobem naszej firmy są ludzie ją tworzący – to dzięki nim realizujemy z sukcesem projekty dla naszych klientów! Jeżeli masz doświadczenie w tworzeniu aplikacji internetowych i mobilnych jako: sales manager, project manager, software architect, frontend developer, backend developer, tester, UI/UX designer, wdrożeniowiec – prosimy o kontakt z naszą firmą i przesłanie swojej propozycji współpracy. Do usłyszenia!
                    </div>

                </div>
            </div>

    </div>
</section>
